# Docker stack
Stack de servicios docker para ejecución de microservicio [farmacias-api](https://gitlab.com/glpunk/farmacias-api) y [farmacias-ui](https://gitlab.com/glpunk/farmacias-ui)

### Requisitos
Docker instalado y máquina host configurado como swarm.

más información: https://docs.docker.com/engine/swarm/

### Instalación
Construir imágenes de [farmacias-api](https://gitlab.com/glpunk/farmacias-api) y [farmacias-ui](https://gitlab.com/glpunk/farmacias-ui), instrucciones en los repositorios correspondientes.

Ejecutar el comando: 

```
docker stack deploy -c dev.yml farmacias-stack 
```

### Servicios
Revisar el estado de los servicios
```
docker service ls
```

### Ingreso
Ingresar a la interfaz con la dirección http://127.0.0.1/ o http://172.17.0.1/ dependiendo de la configuración docker. 