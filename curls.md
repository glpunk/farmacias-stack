### Consulta por región
```
curl -X GET -H "Cache-Control: no-cache" "http://127.0.0.1:3000/stores?id_region=7"
```

### Consulta por región y sector 
```
curl -X GET -H "Cache-Control: no-cache" "http://127.0.0.1:3000/stores?id_region=7&sector=navia"
```
### Consulta por región y nombre 
```
curl -X GET -H "Cache-Control: no-cache" "http://127.0.0.1:3000/stores?id_region=7&name=salco"
```
### Los 3 filtros combinados
```
curl -X GET -H "Cache-Control: no-cache" "http://127.0.0.1:3000/stores?id_region=7&sector=reina&name=salco"
```